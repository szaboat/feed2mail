from unittest import TestCase

import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), "..")))
import feedmailer


class TestFeedMailerTestCase(TestCase):
    def test_init(self):
        feed_mailer = feedmailer.FeedMailer("test@example.com", ("sample", "feeds"))
        self.assertEqual(feed_mailer.email, "test@example.com")
        self.assertEqual(feed_mailer.feed_list, ("sample", "feeds"))
