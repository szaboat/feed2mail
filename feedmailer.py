import datetime

import feedparser
import html2text
import os

from mailer import send_email
from typing import List


smtp_password = os.environ.get("SMTP_PASS")


MAIL_TEMPLATE = """\
# {title}

{content}"""


CONTENT_TEMPLATE = """\
## {content_title} {link}

{content_body}
"""


class FeedMailer:
    def __init__(self, email: str, feed_list: List):
        self.email = email
        self.feed_list = feed_list

    def mail_feeds(self):
        mail = ""
        updated_feeds = set()
        for feed in self.feed_list:
            need_to_send = False
            try:
                parsed_feed = feedparser.parse(feed)
                title = parsed_feed["feed"]["title"]
                yesterday = datetime.date.today() - datetime.timedelta(days=1)
                content_list = []
                for entry in parsed_feed.get("entries", []):
                    published = entry["updated_parsed"]
                    published_date = datetime.date(
                        published.tm_year, published.tm_mon, published.tm_mday
                    )
                    if yesterday == published_date:
                        need_to_send = True
                        content = entry.get('content')
                        if content:
                            content_value = content[0]['value']
                        else:
                            content_value = entry.get('summary')

                        content_list.append(
                            (entry.title, html2text.html2text(content_value), entry.get('link'))
                        )
                        updated_feeds.add(title)
            except Exception as e:
                print(e)

            if need_to_send:
                formatted_content_list = []
                for content in content_list:
                    formatted_content_list.append(
                        CONTENT_TEMPLATE.format(
                            content_title=content[0], content_body=content[1], link=content[2]
                        )
                    )

                mail_content = MAIL_TEMPLATE.format(
                    title=title.upper(), content="".join(formatted_content_list)
                )

                mail+=mail_content

        if mail:
            updated_feeds_content = "# UPDATED FEEDS\n" + "\n".join(updated_feeds)
            full_mail = updated_feeds_content + "\n\n"
            full_mail += mail 
            self._send_email(self.email, full_mail)

    def _send_email(self, recipient: str, content: str):
        send_email(
            "attila.szabo@gmail.com", smtp_password, recipient, "Feed2Mail %s" % datetime.date.today(), content
        )
